import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { PageLayout } from "./Common/PageLayout";
import { About } from "./pages/About";
import { BookList } from "./pages/BookList";
import { Dashboard } from "./pages/Dashboard";
import { Login } from "./pages/Login";
import { PageNotFound } from "./pages/PageNotFound";
import { ProtectedRoute } from "./ProtectedRoute";

function App() {
	return (
		<>
			<Router>
				<Routes>
					<Route path="/" element={<PageLayout />}>
						<Route path="/about" element={<About />} />
						<Route path="/login" element={<Login />} />

						<Route element={<ProtectedRoute />}>
							<Route path="/dashboard" element={<Dashboard />}>
								<Route path="books" element={<BookList />} />
							</Route>
						</Route>

						<Route path="*" element={<PageNotFound />} />
					</Route>
				</Routes>
			</Router>
		</>
	);
}

export default App;

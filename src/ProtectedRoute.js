import React from "react";
import { Navigate, Outlet } from "react-router-dom";

export const ProtectedRoute = () => {
	const userToken = JSON.parse(localStorage.getItem("user"));
	if (userToken) {
		return <Outlet />;
	} else {
		return <Navigate to="/login" replace />;
	}
};

import React from "react";

export const Input = ({
	label,
	type,
	name,
	value,
	handleChange,
	className,
}) => {
	return (
		<>
			<label>{label}</label>
			<input
				type={type}
				name={name}
				value={value}
				onChange={handleChange}
				className={className}
			/>
		</>
	);
};

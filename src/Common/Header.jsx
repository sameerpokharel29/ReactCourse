import React from "react";
import { Link } from "react-router-dom";

export const Header = () => {
	return (
		<nav class="navbar navbar-expand-lg bg-light">
			<div class="container-fluid">
				<a class="navbar-brand" href="/">
					LMS
				</a>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<Link class="nav-link active" aria-current="page" to="/">
								Home
							</Link>
						</li>
						<li class="nav-item">
							<Link class="nav-link" to="/about">
								About
							</Link>
						</li>
						{/* <li class="nav-item">
							<Link class="nav-link" to="/books">
								Books
							</Link>
						</li> */}

						<li class="nav-item">
							<Link class="nav-link" to="/login">
								Login
							</Link>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
};

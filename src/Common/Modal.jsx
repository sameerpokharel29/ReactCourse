import React from "react";
import { Modal, ModalBody, ModalHeader } from "reactstrap";

export const CustomModal = ({
	isOpen,
	toggle,
	size,
	modalHeader,
	modalBody,
}) => {
	return (
		<Modal isOpen={isOpen} toggle={toggle} size={size}>
			<ModalHeader toggle={toggle}>{modalHeader}</ModalHeader>
			<ModalBody>{modalBody}</ModalBody>
		</Modal>
	);
};

import React from "react";
import { Outlet, useLocation } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BookContextProvider } from "../context/BookContext";
import { Footer } from "./Footer";
import { Header } from "./Header";

export const PageLayout = () => {
	let location = useLocation();
	return (
		<>
			<BookContextProvider>
				{location.pathname.includes("/dashboard") ? null : <Header />}
				{location.pathname === "/" && (
					<div className="text-center mt-3">Home Page</div>
				)}
				<Outlet />
				<ToastContainer
					hideProgressBar="true"
					position="bottom-left"
					autoClose={2000}
					pauseOnHover
					theme="light"
				/>

				{location.pathname.includes("/dashboard") ? null : <Footer />}
			</BookContextProvider>
		</>
	);
};

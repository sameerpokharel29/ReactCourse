import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

export const Login = () => {
	const [userData, setUserData] = useState(null);
	let navigate = useNavigate();
	const handleChange = (e) => {
		const { name, value } = e.target;
		setUserData((prevState) => ({ ...prevState, [name]: value }));
	};
	const handleSubmit = (e) => {
		e.preventDefault();
		if (userData === null) {
			toast.error("Email/password is required field");
		} else if (Object.keys(userData).length < 2) {
			toast.error("Email/password is required field");
		} else {
			if (
				userData.email === "admin@admin.com" &&
				userData.password === "Test@1234"
			) {
				localStorage.setItem("user", JSON.stringify(userData));
				toast.success("User Login Successfully!..");
				navigate("/dashboard");
			} else {
				toast.error("User Credentials doesn't match");
			}
		}
	};

	return (
		<>
			<form onSubmit={handleSubmit}>
				<div className="container">
					<div class="form-group">
						<label for="exampleInputEmail1">Email address</label>
						<span className="text-danger">*</span>
						<input
							type="email"
							class="form-control"
							onChange={handleChange}
							name="email"
							// required
						/>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<span className="text-danger">*</span>
						<input
							type="passsword"
							class="form-control"
							onChange={handleChange}
							name="password"
							// required
						/>
					</div>
					<div className="justify-content-end d-flex">
						<button className="btn btn-secondary btn-md mt-2" type="submit">
							Login
						</button>
					</div>
				</div>
			</form>
		</>
	);
};

import { ArcElement, Chart as ChartJS, Legend, Tooltip } from "chart.js";
import React, { useState } from "react";
// import { Pie } from "react-chartjs-2";
import { Link, Outlet, useLocation } from "react-router-dom";
import "./dashboard.css";
ChartJS.register(ArcElement, Tooltip, Legend);

export const data = {
	labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
	datasets: [
		{
			label: "# of Votes",
			data: [12, 19, 3, 5, 2, 3],
			backgroundColor: [
				"rgba(255, 99, 132, 0.2)",
				"rgba(54, 162, 235, 0.2)",
				"rgba(255, 206, 86, 0.2)",
				"rgba(75, 192, 192, 0.2)",
				"rgba(153, 102, 255, 0.2)",
				"rgba(255, 159, 64, 0.2)",
			],
			borderColor: [
				"rgba(255, 99, 132, 1)",
				"rgba(54, 162, 235, 1)",
				"rgba(255, 206, 86, 1)",
				"rgba(75, 192, 192, 1)",
				"rgba(153, 102, 255, 1)",
				"rgba(255, 159, 64, 1)",
			],
			borderWidth: 1,
		},
	],
};

export const Dashboard = () => {
	let location = useLocation();
	const [active, setActive] = useState(0);
	return (
		<>
			<div class="sidebar">
				<Link
					to="/dashboard"
					onClick={() => setActive(0)}
					className={`${active === 0 && "active"}`}
				>
					<span>Dashboard</span>
				</Link>
				<Link
					className={`${active === 1 && "active"}`}
					onClick={() => setActive(1)}
					to="/dashboard/books"
				>
					<span>Books</span>
				</Link>
				<Link
					className={`${active === 3 && "active"}`}
					onClick={() => setActive(3)}
					to="/dashboard/books"
				>
					<span>New Module</span>
				</Link>
				<Link
					to="/"
					onClick={() => {
						localStorage.removeItem("user");
					}}
				>
					<span>Logout</span>
				</Link>
			</div>

			<div class="content">
				{location.pathname === "/dashboard" && (
					<div class="helper">
						<span className="mt-5 d-flex justify-content-center">Welcome Admin</span>
					</div>
				)}
				<Outlet />
			</div>
		</>
	);
};

import React, { useState } from "react";
import { toast } from "react-toastify";
import { Input } from "../../Common/Input";
import { CustomModal } from "../../Common/Modal";
import { useBookData } from "../../context/BookContext";

const BookForm = ({
	bookToBeUpdated,
	setBookToBeUpdated,
	dispatch,
	toggle,
}) => {
	const [formData, setFormData] = useState({ status: false });

	const handleChange = (e) => {
		const { name, value, type, checked } = e.target;
		if (bookToBeUpdated === null) {
			setFormData((prevState) => ({
				...prevState,
				[name]: type === "checkbox" ? checked : value,
			}));
		} else {
			//make changes to the bookTobeUpdated State
			setBookToBeUpdated((prevState) => ({
				...prevState,
				[name]: type === "checkbox" ? checked : value,
			}));
		}
	};
	const handleSubmit = (e) => {
		e.preventDefault();
		if (bookToBeUpdated) {
			const { publication, date, ...reqBookData } = bookToBeUpdated;
			if (Object.values(reqBookData).includes("")) {
				toast.error("Missing field value");
				return;
			} else {
				dispatch({ type: "UPDATE", payload: reqBookData });
				setBookToBeUpdated(null);
				toast.success("Book Updated Succesfully");
			}
		} else {
			if (Object.keys(formData).length < 4)
				return toast.error("All field is required");
			dispatch({ type: "CREATE", payload: formData });
			setFormData({ status: false });
			toast.success("Book Created Succesfully");
		}
		toggle();
	};
	return (
		<form className="" onSubmit={handleSubmit}>
			<div className="form-group">
				<Input
					label="Book Title"
					type="text"
					value={bookToBeUpdated ? bookToBeUpdated?.title : formData?.title}
					name="title"
					className="form-control"
					handleChange={handleChange}
				/>
			</div>
			<div className="form-group">
				<label for="exampleInputPassword1">Description</label>
				<textarea
					type="text"
					value={
						bookToBeUpdated ? bookToBeUpdated?.description : formData?.description
					}
					name="description"
					onChange={handleChange}
					className="form-control"
				/>
			</div>
			<div className="form-group">
				<label for="exampleInputPassword1">Author</label>
				<input
					type="text"
					value={bookToBeUpdated ? bookToBeUpdated?.author : formData?.author}
					className="form-control"
					name="author"
					onChange={handleChange}
				/>
			</div>

			<div className="form-group form-check">
				<input
					type="checkbox"
					className="form-check-input"
					name="status"
					onChange={handleChange}
					checked={bookToBeUpdated ? bookToBeUpdated?.status : formData.status}
				/>
				<label className="form-check-label" for="exampleCheck1">
					Status
				</label>
			</div>
			<div className="justify-content-end d-flex">
				<button type="submit" className="btn btn-primary text-end">
					{bookToBeUpdated ? "Update" : "Submit"}
				</button>
			</div>
		</form>
	);
};

export const BookList = () => {
	const { state: data, dispatch } = useBookData();
	const [modal, setModal] = useState(false);
	const [bookToBeUpdated, setBookToBeUpdated] = useState(null);
	const toggle = () => {
		modal && setBookToBeUpdated(null);
		setModal(!modal);
	};

	return (
		<>
			<div className="container mt-4">
				<h3 className="text-center lead">Books</h3>
				<div
					style={{
						justifyContent: "end",
						display: "flex",
						marginRight: "1rem",
					}}
				>
					<button type="button" className="btn btn-success" onClick={toggle}>
						Create New Book
					</button>
				</div>
				<div className="">
					<table class="table">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Title</th>
								<th scope="col">Description</th>
								<th scope="col">Author Name</th>
								<th scope="col">Status</th>
								<th scope="col">Actions</th>
							</tr>
						</thead>
						<tbody>
							{data.bookData.length > 0 ? (
								data.bookData.map((el, index) => (
									<tr key={el.id}>
										<th scope="row">{index + 1}</th>
										<td>{el.title}</td>
										<td>{el.description}</td>
										<td>{el.author}</td>
										<td>{el.status ? "Available" : "Not Available"} </td>
										<td>
											<button
												type="button"
												className="btn btn-primary"
												onClick={() => {
													setModal(true);
													setBookToBeUpdated(el);
												}}
											>
												Edit
											</button>
											<button
												type="button"
												className="btn btn-danger ms-1"
												onClick={() => {
													dispatch({ type: "DELETE", payload: el.id });
													toast.success("Book Deleted Successfully");
												}}
											>
												Delete
											</button>
										</td>
									</tr>
								))
							) : (
								<tr>
									<td colspan={6} className="text-center">
										No Books Found
									</td>
								</tr>
							)}
						</tbody>
					</table>
				</div>
			</div>

			<div>
				<CustomModal
					isOpen={modal}
					toggle={toggle}
					size="lg"
					modalHeader={bookToBeUpdated ? "Edit Book" : "Create Book"}
					modalBody={
						<BookForm
							bookToBeUpdated={bookToBeUpdated}
							setBookToBeUpdated={setBookToBeUpdated}
							dispatch={dispatch}
							toggle={toggle}
						/>
					}
				/>
			</div>
		</>
	);
};
